/**
 * Display a list of vehicles. It shows ONLY the vehicles owned by the user that 
 * is logged in.
 */

(function() {
  'use strict';

  angular
    .module('vehicle-listing.controller', ['ui.router'])
    .config(function($stateProvider) {
      $stateProvider
        .state('auth.vehicle-listing', {
          url: '/vehicle-listing',
          templateUrl: 'app/views/vehicle-listing.html',
          controller: 'VehicleListingController as vm',
          resolve: {
            vehicles: ['UserService', function(UserService) {
              const userId = localStorage.getItem('user_id');
              return UserService.getUserCars(userId);
            }],
            load: ['CSSInjector', function(CSSInjector) {
              return CSSInjector.set("vehicle-listing", "vehicle-listing.css");
            }]
          },
          data: {
            pageTitle: 'Mis Vehículos',
            background: 'cars'
          }
        });
    })
    .controller('VehicleListingController', VehicleListingController);

  VehicleListingController.$inject = ['vehicles'];

  function VehicleListingController(vehicles) {
    let vm = this;
    vm.vehicles = vehicles;

    vm.hasVehicles = vehicles.length >= 1;
  }

})();
