/**
 * Display a list of reservations. It shows ONLY the reservations made by the 
 * user that is logged in.
 */

(function() {
  'use strict';

  angular
    .module('reservation-listing.controller', ['ui.router'])
    .config(function($stateProvider) {
      $stateProvider
        .state('auth.reservation-listing', {
          url: '/reservation-listing',
          templateUrl: 'app/views/reservation-listing.html',
          controller: 'ReservationListingController as vm',
          resolve: {
            reservations: ['ReservationService', function(ReservationService) {
              const userId = localStorage.getItem('user_id');
              return ReservationService.getMyReservations(userId);
            }],
            load: ['CSSInjector', function(CSSInjector) {
              return CSSInjector.set("reservation-listing", "reservation-listing.css");
            }]
          },
          data: {
            pageTitle: 'Mis Reservaciones',
            background: 'cars'
          }
        });
    })
    .controller('ReservationListingController', ReservationListingController);

  ReservationListingController.$inject = ['reservations'];

  function ReservationListingController(reservations) {
    let vm = this;
    vm.reservations = reservations;

    vm.hasReservations = reservations.length >= 1;
  }

})();
