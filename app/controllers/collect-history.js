/**
 * Display the user collect history.
 * The user must be logged in in order to access this state.
 */

(function() {
  'use strict';

  angular
    .module('collect-history.controller', ['ui.router'])
    .config(function($stateProvider) {
      $stateProvider
        .state('auth.collect-history', {
          url: '/collect-history',
          templateUrl: 'app/views/collect-history.html',
          controller: 'CollectHistoryController as vm',
          data: {
            pageTitle: 'Historial de Cobros',
            background: 'cars'
          },
          resolve: {
            load: ['CSSInjector', function(CSSInjector) {
              return CSSInjector.set("collect-history", "collect-history.css");
            }]
          }
        });
    })
    .controller('CollectHistoryController', CollectHistoryController);

  function CollectHistoryController() {
    let vm = this;

    vm.friendName = "Ricardo";
    vm.fechaComment = "Noviembre 2017";
    vm.friendComment = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of";
    vm.misCalificaciones = "50";
  }

})();