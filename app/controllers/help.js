/**
 * This controller uses help.html to display FAQ.
 */

(function() {
  'use strict';

  angular
    .module('help.controller', ['ui.router'])
    .config(function($stateProvider) {
      $stateProvider
        .state('help', {
          url: '/help',
          templateUrl: 'app/views/help.html',
          controller: 'HelpController as vm',
          data: {
            pageTitle: 'Ayuda',
            background: 'cars'
          },
          resolve: {
            load: ['CSSInjector', function(CSSInjector) {
              return CSSInjector.set("help", "help.css");
            }]
          }
        });
    })
    .controller('HelpController', HelpController);

  function HelpController() {
    let vm = this;
  }

})();
